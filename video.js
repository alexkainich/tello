const spawn = require('child_process').spawn;
const exec = require('child_process').exec;
const path = require('path')
const Jimp = require('Jimp');
const dgram = require('dgram');
const glob = require("glob")
const fs = require('fs-extra')
const robot = require('robotjs');
const TELLO_IP = '192.168.10.1'
const TELLO_PORT = 8889
const stats = ['speed?','battery?','time?','height?','temp?','attitude?','baro?','acceleration?','tof?','wifi?']
const statsLabels = ['Speed','Battery','Air time','Height from takeoff','Temperature','Attitude','Barometer','Acceleration','Floor distance','Wifi']
let statsValues = Array(10).fill('')
let statNumber = stats.length
let pythonChild;
let processedImages=[];
let aiActive = false;

//  1. Initiate tello drone
const udpClient = dgram.createSocket('udp4');
udpClient.bind(TELLO_PORT)
udpClient.send("command", TELLO_PORT, TELLO_IP, null);
udpClient.send("streamon", TELLO_PORT, TELLO_IP, null);

//  2. Begin the mplayer stream. We wait for Tello to connect first
const args = [ 
 '-input', 'conf=inputTello.conf',
 '-gui',
 '-nolirc',
 '-fps', '35',
 '-really-quiet',
 '-vf', 'screenshot',
 '-x', '2000',
 '-y', '1500',
 '-v', 'udp://0.0.0.0:11111'
]
const streamer = spawn('mplayer', args);

// 3. Start python depth analysis
clearAIFiles()
const pythonArgs = [ 
  './MiDaS/run.py',
  '--model_type','midas_v21_small'
 ]
pythonChild = spawn('python', pythonArgs,{detached: true});

// 3. Update stats on file
// udpClient.on('message', (msg, rinfo) => updateStatsFile(msg));
// updatestats()
// setInterval(function() {
//   if (statNumber >= stats.length-1) {
//     updatestats()
//   }
// }, 3000);

/////////////////////////////////////////
/////////////////////////////////////////
/////////////// Methods /////////////////
/////////////////////////////////////////
/////////////////////////////////////////
send = (command) => {
  console.log(command)
	udpClient.send(command, TELLO_PORT, TELLO_IP, null);
}
exports.send = send

closeAll = () => {
  if (pythonChild) pythonChild.kill()
  streamer.kill()
  udpClient.off('message', updateStatsFile)
  udpClient.close()
}
exports.closeAll = closeAll

updateStatsFile = (msg) => {
  statsValues[statNumber] = msg
  if (statNumber >= stats.length-1) {
    writeFile()
  }
}

sleep = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

updatestats = async () => {
  for (statNumber = 0; statNumber < stats.length; statNumber++) {
    send(stats[statNumber]);
    await sleep(250)
  }
}

writeFile = () => {
  let contents = ''
  for (let i = 0; i < stats.length; i++) {
    contents += statsLabels[i] + ': ' + statsValues[i] +'\n'
  }

  fs.writeFile('overlayText.txt', contents, function (err) {
    if (err) return console.log(err)
  });
}

getRandomInt = (max) => {
  return Math.floor(Math.random() * max);
}

AIActivate = async () => {
  aiActive = true
  await AIMove()
}
exports.AIActivate = AIActivate

AIStop = () => {
  aiActive = false
}
exports.AIStop = AIStop

AIMove = async () => {
  while (aiActive) {
    await stopAndTurnDrone()

    if (!aiActive) {
      console.log("Stopping AI")
      return
    }

    const screenshotFile = await takeScreenshot()
    if (!screenshotFile) continue

    if (!aiActive) {
      console.log("Stopping AI")
      return
    }

    const outputFile = await waitForDepthAnalysis()
    if (!outputFile) continue

    if (!aiActive) {
      console.log("Stopping AI")
      return
    }

    const rgb = await getImageRGBData(outputFile)

    if (!aiActive) {
      console.log("Stopping AI")
      return
    }

    await decideMove(rgb)

    console.log("");
  }
}
exports.AIMove = AIMove

takeScreenshot = async () => {
  try {
    console.log("Take screenshot");

    // take mplayer screenshot
    robot.keyTap('>', 'shift');

    // wait for screenshot file
    const srceenshotFile = await waitTillFileExists("*.png")
    if (!srceenshotFile) {
      console.log("Cannot take screenshot.")
    }

    return srceenshotFile
  } catch {
    console.log("Error taking screenshot !")
  }
}

stopAndTurnDrone = async () => {
  console.log("Stop and turn drone");
  send('rc 0 0 0 0') // stop drone
  let leftOrRight = getRandomInt(2) === 0 ? -1 : 1
  send('rc 0 0 0 ' + leftOrRight * 50) // turn drone
  await sleep(getRandomInt(1000) + 500) // wait for 0.5s + random(0s, 1s)
  send('rc 0 0 0 0') // stop drone
}

waitForDepthAnalysis = async () => {
  try {
    const outputFile = await waitTillFileExists("./output/*.png")
    if (!outputFile) {
      console.log("Error in depth analysis.")
    }
    processedImages.push(outputFile)
    return outputFile
  } catch {
    console.log("Error waiting for analysis file !")
  }
}

// decideMove = async (rgb) => {
//   let count = 0
//   for (let x = Math.round(rgb.length/3); x < Math.round(rgb.length*2/3); x++) {
//     for (let y = Math.round(rgb[0].length/2); y < Math.round(rgb[0].length*4/5); y++) {
//       if (rgb[x][y] > 150)
//         count++
//     }
//   }
//   if (count < 30) { //no obstacle so go forward
//     console.log("Moving forward...")
//     send('rc 0 50 0 0')
//     await sleep(1000) // and stop after 1 second
//     send('rc 0 0 0 0')
//   } else {
//     console.log("Obstacle found!");
//   }
// }

decideMove = async (rgb) => {
  const darkThreshold = 30
  const countThreshold = 30
  let darkAreaXMin = 3000, darkAreaXMax = -100
  let count = 0
  for (let x = Math.round(rgb.length/5); x < Math.round(rgb.length*4/5); x++) {
    for (let y = Math.round(rgb[0].length/2); y < Math.round(rgb[0].length*4/5); y++) {
      if (rgb[x][y] < darkThreshold &&
         (x-1 > 0 && rgb[x-1][y] < darkThreshold) ||
         (x+1 < rgb.length-1 && rgb[x+1][y] < darkThreshold) ||
         (y-1 > 0 && rgb[x][y-1] < darkThreshold) ||
         (y+1 < rgb[0].length && rgb[x][y+1] < darkThreshold)) {
          count++
          if (darkAreaXMin > x) darkAreaXMin = x
          if (darkAreaXMax < x) darkAreaXMax = x
        }
    }
  }
  if (count > countThreshold) {
    console.log("Found deep area")
    const centerAreaX = Math.round((darkAreaXMin + darkAreaXMax) / 2)
    const turnDuration = Math.abs(rgb.length - centerAreaX)/rgb.length * 500
    const turnDirection = rgb.length - centerAreaX < 0 ? 1 : -1 
    send('rc 0 0 0 ' + turnDirection * 50)
    await sleep(turnDuration)
    send('rc 0 50 0 0')
    await sleep(1000)
    send('rc 0 0 0 0')
  } else {
    console.log("Wall found!");
  }
}

waitTillFileExists = async (file) => {
  let count = 0;
  let pngFiles = glob.sync(file, { cwd: "." });
  let newPngFiles = pngFiles.filter(x => !processedImages.includes(x));

  while (newPngFiles.length === 0 && count <= 50) {
    pngFiles = glob.sync(file, { cwd: "." });
    newPngFiles = pngFiles.filter(x => !processedImages.includes(x));
    await sleep(200)
    count++
  }

  if (newPngFiles.length === 0) {
    return null;
  } else {
    return newPngFiles[0]
  }
}

async function clearAIFiles() {
  const directory = './output';

  // delete depth analysis images
  await fs.emptyDir(directory)

  // delete old screenshots
  const startDir = './';
  const filter = '.png';
  var files=fs.readdirSync(startDir);
  for(var i=0;i<files.length;i++){
      var filename=path.join(startDir,files[i]);
      var stat = fs.lstatSync(filename);
      if (stat.isDirectory()){
      }
      else if (filename.indexOf(filter)>=0) {
        try {
          await fs.remove(startDir + '/' + filename)
        } catch (err) { }
      }
  };
}

getImageRGBData = async (file) => {
  try {
    let pixels = [];
    let contents = await Jimp.read(file)
    let width = contents.bitmap.width;
    let height = contents.bitmap.height;

    // pixels[x][y]
    for (let x = 0; x < width; x++) {
      let rowPixels = [];
      for (let y = 0; y < height; y++) {
        let pixel = Jimp.intToRGBA(contents.getPixelColor(x, y));
        if (pixel.r !== pixel.g || pixel.r !== pixel.b || pixel.g !== pixel.b) {
          console.log("WARNING: Pixel data looks wrong !")
        }
        rowPixels.push(pixel.r);
      }
      pixels.push(rowPixels);
    }

    // fs.writeFile('aktest.json', JSON.stringify({ data: pixels }), 'utf8', (err) => {
    //   if (err) { throw err; }
    // });

    return pixels;
  } catch {
    console.log("Error getting RBG data !")
  }
}