const Jimp = require('Jimp');
const fs = require('fs-extra')
const glob = require("glob")

let path = './output/*.png';
AIDecision(path)

async function AIDecision(path) {
  let pngFiles = glob.sync('./output/*.png', { cwd: "." });
  for (let x = 0; x < pngFiles.length; x++) {
    let rgb = await getImageRGBData(pngFiles[x])
    let count = 0

    for (let x = Math.round(rgb.length/3); x < Math.round(rgb.length*2/3); x++) {
      for (let y = Math.round(rgb[0].length/2); y < Math.round(rgb[0].length*4/5); y++) {
        if (rgb[x][y] > 150)
          count++
      }
    }
    if (count < 30) { //no obstacle so go forward
      console.log(pngFiles[x] + " Move forward!")
    } else {
      console.log(pngFiles[x] + " Obstacle found!");
    }
  }
}

async function getImageRGBData(file) {
  let pixels = [];
  let contents = await Jimp.read(file)
  let width = contents.bitmap.width;
  let height = contents.bitmap.height;

  // pixels[x][y]
  for (let x = 0; x < width; x++) {
    let rowPixels = [];
    for (let y = 0; y < height; y++) {
      let pixel = Jimp.intToRGBA(contents.getPixelColor(x, y));
      if (pixel.r !== pixel.g || pixel.r !== pixel.b || pixel.g !== pixel.b) {
        console.log("WARNING: Pixel data looks wrong !")
      }
      rowPixels.push(pixel.r);
    }
    pixels.push(rowPixels);
  }

  fs.writeFile('aktest.json', JSON.stringify({ data: pixels }), 'utf8', (err) => {
    if (err) { throw err; }
  });

  return pixels;
}