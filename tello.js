const video = require('./video.js');
//const image = require('./image.js')
const ioHook = require('iohook');
const eol = require('os').EOL;
const keyMap = new Map();
keyMap.set('h', 'help');
keyMap.set('return', 'takeoff');
keyMap.set('escape', 'land');
keyMap.set('up', 'forward');
keyMap.set('down', 'back');
keyMap.set('left', 'left');
keyMap.set('right', 'right');
keyMap.set('w', 'up');
keyMap.set('s', 'down');
keyMap.set('a', 'turnleft');
keyMap.set('d', 'turnright');
keyMap.set('m', 'AI enable / disable');
const eventMap = new Map();
eventMap.set(28, 'return');
eventMap.set(1, 'escape');
eventMap.set(61000, 'up');
eventMap.set(61008, 'down');
eventMap.set(61003, 'left');
eventMap.set(61005, 'right');
eventMap.set(17, 'w');
eventMap.set(31, 's');
eventMap.set(30, 'a');
eventMap.set(32, 'd');
let movePriority = [];
let keysPressed = [];
let lastCommand;
let aiActive = false;

// Register keyup and keydown events and show help
ioHook.on("keydown", event => keyDown(event))
ioHook.on("keyup", event => keyUp(event))
ioHook.start();

console.log('---------------------------------------');
console.log('Tello Command Console');
console.log('---------------------------------------');
console.log('Press a key to send a command to Tello');
listKeys();

// Methods
function keyDown(event) {
	if (event.keycode == 46) { // "c" key
		send('land')
		ioHook.removeListener('keydown', keyDown)
		ioHook.removeListener('keyup', keyUp)
		video.closeAll()
		process.exit()
	} else if (event.keycode == 42 || event.keycode == 52 || event.keycode == 29) { // "shift", ".", "ctrl"
		// do nothing. This is "shift ." to take photo from mplayer
		// Also 'ctrl' (29). This is for 'ctrl - c' to work too
	} else if (event.keycode == 50) { // "m" key
		if (aiActive === false) {
			aiActive = true
			video.AIActivate();
		} else {
			aiActive = false
			video.AIStop();
		}
	} else if (event.keycode === 35) { // "h" key
		listKeys();
	} else {
		if (eventMap.has(event.keycode)) {
			let keyPressed = eventMap.get(event.keycode);
			keysPressed.push(keyPressed)
			sendCommand(keyPressed);
		} else {
			console.log(`No symbol defined for "${event.keycode}" key.`);
		}
	}
}

function keyUp(event) {
	if (event.keycode == 61000 || event.keycode == 61003 || event.keycode == 61005 || event.keycode == 61008 ||
		event.keycode == 17 || event.keycode == 30 || event.keycode == 31 || event.keycode == 32) {
			let keyReleased = eventMap.get(event.keycode)
			keysPressed = keysPressed.filter((e) => e !== keyReleased)
			sendCommand();
	}
}

function sendCommand(command) {
	switch (command) {			
		case ' ':
			send('streamon');
			return;
		case 'return':
			send('takeoff');
			return;
		case 'escape':
			send('land')
			return;			
		case 'z':
			send('battery?')
			return;
		case 'x':
			send('time?')
			return;							
	}

	let upMove = keysPressed.find(k => k === 'up') ? true : false
	let downMove = keysPressed.find(k => k === 'down') ? true : false
	let leftMove = keysPressed.find(k => k === 'left') ? true : false
	let rightMove = keysPressed.find(k => k === 'right') ? true : false
	let wMove = keysPressed.find(k => k === 'w') ? true : false
	let sMove = keysPressed.find(k => k === 's') ? true : false
	let aMove = keysPressed.find(k => k === 'a') ? true : false
	let dMove = keysPressed.find(k => k === 'd') ? true : false

	udlrCalc = movementCalc(upMove, downMove, leftMove, rightMove)
	wsadCalc = movementCalc(wMove, sMove, aMove, dMove)
	
	command = `rc ${100 * udlrCalc[0]} ${100 * udlrCalc[1]} ${100 * wsadCalc[1]} ${100 * wsadCalc[0]}`

	if (command !== lastCommand) {
		send(command);
		lastCommand = `rc ${100 * udlrCalc[0]} ${100 * udlrCalc[1]} ${100 * wsadCalc[1]} ${100 * wsadCalc[0]}`
	}
}

function listKeys() {
  console.log(`${eol}keys`);
  keyMap.forEach((value, key) => {
    console.log(`${key} - ${value}`);
  });
  console.log();
}

function send(command) {
	video.send(command)
}

function movementCalc(u, d, l, r) {
	let newPosX = 0
    let newPosY = 0

    if(u && !movePriority.includes('up')) movePriority.push('up')
    else if(!u) movePriority = movePriority.filter((e) => e !== 'up')

    if(d && !movePriority.includes('down')) movePriority.push('down')
    else if(!d) movePriority = movePriority.filter((e) => e !== 'down')

    if(l && !movePriority.includes('left')) movePriority.push('left')
    else if(!l) movePriority = movePriority.filter((e) => e !== 'left')

    if(r && !movePriority.includes('right')) movePriority.push('right')
    else if(!r) movePriority = movePriority.filter((e) => e !== 'right')

    if(u && (!movePriority.includes('down') || movePriority.indexOf('up') > movePriority.indexOf('down'))) {
      newPosY += 1
    }
    if(d && (!movePriority.includes('up') || movePriority.indexOf('down') > movePriority.indexOf('up'))) {
      newPosY -= 1
    }
    if(l && (!movePriority.includes('right') || movePriority.indexOf('left') > movePriority.indexOf('right'))) {
      newPosX -= 1
    }
    if(r && (!movePriority.includes('left') || movePriority.indexOf('right') > movePriority.indexOf('left'))) {
      newPosX += 1
    }

    if (newPosX !== 0 && newPosY !== 0) {
      newPosX = newPosX * 0.7071
      newPosY = newPosY * 0.7071
    }

    return [newPosX, newPosY]
}