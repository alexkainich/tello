Needs node v13.0.0

1. Install mplayer
2. `npm install`
3. inputTello.conf must be placed in the mplayer folder (in ~/)
4. Connect to Tello
5. `node tello.js`
6. The camera window must remain focused and active